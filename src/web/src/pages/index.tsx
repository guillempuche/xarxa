import React from "react";

import Layout from "../components/Layout";
import Content from "../components/Content";

const Index: React.FC = () => {
  return (
    <Layout>
      <Content />
    </Layout>
  );
};

export default Index;
