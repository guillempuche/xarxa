import React from "react";

import Language from "./Language";

const TopBar: React.FC = () => {
  return (
    <div>
      <Language />
    </div>
  );
};

export default TopBar;
